package bg.sofia.uni.fmi.mjt.carstore.car;

import java.util.Random;

import bg.sofia.uni.fmi.mjt.carstore.enums.Region;

public class RegistrationNumberGenerator {
	private Region region;

	public char getRandomChar() {
		Random random = new Random(); 
		return (char)(random.nextInt(26) + 'A');
	}
	
	public String generateRegistrationNumber(Region region) {
		this.region = region;
		String registrationNumber = region.getPrefix() + region.getNumber() + getRandomChar() + getRandomChar();
		region.increaseNumber();
		return registrationNumber;
	}
}
