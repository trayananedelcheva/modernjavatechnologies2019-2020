package bg.sofia.uni.fmi.mjt.carstore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import bg.sofia.uni.fmi.mjt.carstore.car.Car;
import bg.sofia.uni.fmi.mjt.carstore.enums.Model;
import bg.sofia.uni.fmi.mjt.carstore.exception.CarNotFoundException;

public class CarStore {
	
	private Set<Car> cars = new TreeSet<Car>();
	private int totalPrice = 0;
	
	public boolean add(Car car) {
		if(cars.contains(car)) {
			return false;
		}
		totalPrice = totalPrice + car.getPrice();
		return cars.add(car);
	}
	
	/*public boolean addAll(Collection<Car> newSetOfCars) {
		Set<Car> oldSetOfCars = new TreeSet<Car>(cars);
		cars.addAll(newSetOfCars);
		if(oldSetOfCars.containsAll(newSetOfCars)) {
			return false;
		}
		return true;
	}*/
	
	public boolean addAll(Collection<Car> newSetOfCars) {
		boolean isAdded = false; 
		for(Car car: newSetOfCars) {
			boolean checkIfIsAddedNewCar = cars.add(car);
			if(checkIfIsAddedNewCar) {
				totalPrice = totalPrice + car.getPrice();
				isAdded = true;
			}
		}
		return isAdded;
	}
	
	public boolean remove(Car car) {
		if(!(cars.contains(car))) {
			return false;
		}
		totalPrice = totalPrice - car.getPrice();
		return cars.remove(car);
	}
	
	public Collection<Car> getCarsByModel(Model model) {
		Set<Car> sortedCarsByModelAndYear = new TreeSet<>(new DefaultComparator());
		for(Car current: cars) {
			if(model == current.getModel()) {
				sortedCarsByModelAndYear.add(current);
			}
		}
		return sortedCarsByModelAndYear;
	}	
	
	public Car getCarByRegistrationNumber(String registrationNumber) {
		for(Car current: cars) {
			if(registrationNumber == current.getRegistrationNumber()) {
				return current;
			}
		}
		String message = String.format("Car with this registration number %s not found", registrationNumber);
		throw new CarNotFoundException(message);
	}
	
	public Collection<Car> getCars() {
		Set<Car> carsSortedByDefaultOrder = new TreeSet<Car>(new DefaultComparator());
		for(Car current: cars) {
			carsSortedByDefaultOrder.add(current);
		}
		return carsSortedByDefaultOrder;
	}
	
	public Collection<Car> getCars(Comparator<Car> comparator) {
		TreeSet<Car> sortedCarsBySpecifiedComparator = new TreeSet<Car>(comparator); 
		sortedCarsBySpecifiedComparator.addAll(cars);
		return sortedCarsBySpecifiedComparator;
	}
	
	public Collection<Car> getCars(Comparator<Car> comparator, boolean isReversed) {
		if (isReversed == true) {
			return getCars(Collections.reverseOrder(comparator));
		}
		return getCars(comparator);
	}
	
	public Collection<Car> getCarsReversed(Comparator<Car> comparator, boolean isReversed) {
		TreeSet<Car> sortedCarsBySpecifiedComparator = new TreeSet<Car>(comparator); 
		for(Car car: cars) {
			sortedCarsBySpecifiedComparator.add(car);
		}
		List<Car> list = new ArrayList<Car>(sortedCarsBySpecifiedComparator);
		Collections.sort(list, comparator);
		Collections.reverse(list);
		return list;
 	}
	
	public int getNumberOfCars() {
		return cars.size();
	}
	
	public int getTotalPriceForCars() {
		return totalPrice;
	}
}
