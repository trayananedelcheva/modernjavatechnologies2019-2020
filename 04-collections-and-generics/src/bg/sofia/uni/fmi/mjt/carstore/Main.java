package bg.sofia.uni.fmi.mjt.carstore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import bg.sofia.uni.fmi.mjt.carstore.car.Car;
import bg.sofia.uni.fmi.mjt.carstore.car.OrdinaryCar;
import bg.sofia.uni.fmi.mjt.carstore.enums.EngineType;
import bg.sofia.uni.fmi.mjt.carstore.enums.Model;
import bg.sofia.uni.fmi.mjt.carstore.enums.Region;

public class Main {
	public static void main(String[] args) {
		OrdinaryCar ordinaryCar1 = new OrdinaryCar(Model.AUDI, 2004, 1100, EngineType.DIESEL, Region.BURGAS);
		OrdinaryCar ordinaryCar2 = new OrdinaryCar(Model.ALFA_ROMEO, 2001, 1200, EngineType.GASOLINE, Region.RUSE);
		OrdinaryCar ordinaryCar3 = new OrdinaryCar(Model.BMW, 2011, 5300, EngineType.DIESEL, Region.BURGAS);
		OrdinaryCar ordinaryCar4 = new OrdinaryCar(Model.FERRARI, 2018, 250000, EngineType.ELECTRIC, Region.RUSE);
		OrdinaryCar ordinaryCar5 = new OrdinaryCar(Model.MERCEDES, 2019, 300000, EngineType.HYBRID, Region.VIDIN);
		OrdinaryCar ordinaryCar6 = new OrdinaryCar(Model.AUDI, 1999, 900, EngineType.GASOLINE, Region.SOFIA);
		OrdinaryCar ordinaryCar7 = new OrdinaryCar(Model.MERCEDES, 2009, 4000, EngineType.GASOLINE, Region.SOFIA);
		/*System.out.println(ordinaryCar1.getRegistrationNumber()); 
		System.out.println(ordinaryCar2.getRegistrationNumber());
		System.out.println(ordinaryCar3.getRegistrationNumber());
		System.out.println(ordinaryCar4.getRegistrationNumber());
		Set<Car> cars1 = new TreeSet<Car>(); 
		cars1.add(ordinaryCar1); 
		cars1.add(ordinaryCar2); 
		cars1.add(ordinaryCar3);
		cars1.add(ordinaryCar4);
		Set<Car> cars2 = new TreeSet<Car>(); 
		cars2.add(ordinaryCar1);
		cars2.add(ordinaryCar2); 
		cars2.add(ordinaryCar3);
		System.out.println(ordinaryCar1.equals(ordinaryCar1));
		Set<Car> cars3 = new TreeSet<Car>(cars2);
		for(Car car : cars3) {
			System.out.println(car);
		}
		System.out.println("--------------------------------");
		Set<Car> cars4 = new TreeSet<Car>(); 
		cars4.add(ordinaryCar1); 
		cars3.addAll(cars1);
		for(Car car : cars3) {
			System.out.println(car);
		}
		boolean check = cars3.containsAll(cars4);
		System.out.println("Check if the store contains all the cars from the new Collection: " + check);
		boolean checkIfCarsAreAlreadyInStore = cars2.containsAll(cars1);
		System.out.println(checkIfCarsAreAlreadyInStore);*/
		CarStore carStore = new CarStore();
		carStore.add(ordinaryCar1); 
		carStore.add(ordinaryCar2); 
		carStore.add(ordinaryCar3);
		Set<Car> newSetOfCars = new TreeSet<Car>();
		newSetOfCars.add(ordinaryCar1); 
		newSetOfCars.add(ordinaryCar2); 
		newSetOfCars.add(ordinaryCar3);
		boolean result = carStore.addAll(newSetOfCars);
		System.out.println(result);
		System.out.println("-------------------------------------");
		Set<Car> cars = new TreeSet<Car>(); 
		cars.add(ordinaryCar1);
		cars.add(ordinaryCar2);
		cars.add(ordinaryCar3);
		cars.add(ordinaryCar4);
		cars.add(ordinaryCar5);
		cars.add(ordinaryCar6);
		cars.add(ordinaryCar7);
		Set<Car> sortedByModelAndYear = new TreeSet<Car>(new DefaultComparator());
		for(Car current : cars) {
			if(current.getModel() == Model.MERCEDES) {
				sortedByModelAndYear.add(current);
			}
		}
		for(Car c: sortedByModelAndYear) {
			System.out.println(c);
		}
		carStore.add(ordinaryCar4);
		carStore.add(ordinaryCar5);
		carStore.add(ordinaryCar6);
		carStore.add(ordinaryCar7);
		/*System.out.println("---------------------------------");
		
		DefaultComparator comparator = new DefaultComparator();
		Collection<Car> sortedCarsBySpecifiedComparator = carStore.getCars(comparator);
		for(Car car: sortedCarsBySpecifiedComparator) {
			System.out.println(car);
		}
		System.out.println("---------------------------------");
		Collection<Car> sortedCarsInReversedOrder = carStore.getCars(comparator, true);
		for(Car car: sortedCarsInReversedOrder) {
			System.out.println(car);
		}
		System.out.println("--------------------------------");
		Set<Car> sortedSet = new TreeSet<Car>(comparator);
		for(Car car: cars) {
			sortedSet.add(car);
		}
		List<Car> list = new ArrayList<Car>(sortedSet);
		Collections.reverse(list);
		for(Car car: list) {
			System.out.println(car);
		}*/
		DefaultComparator comparator = new DefaultComparator();
		System.out.println("--------------------------------");
		Collection<Car> carsReversed = carStore.getCarsReversed(comparator, true);
		for(Car car: carsReversed) {
			System.out.println(car);
		}
		boolean isAddedNewCar = newSetOfCars.addAll(cars);
		System.out.println(isAddedNewCar);
	}
}
