package bg.sofia.uni.fmi.mjt.carstore;

import java.util.Comparator;

import bg.sofia.uni.fmi.mjt.carstore.car.Car;

public class DefaultComparator implements Comparator<Car> {

	@Override
	public int compare(Car one, Car two) {
		int c; 
		c = one.getModel().compareTo(two.getModel()); 
		if(c == 0) {
			c = Integer.compare(one.getYear(), two.getYear());
		}
		return c;
	}
}