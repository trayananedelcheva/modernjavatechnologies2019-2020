package bg.sofia.uni.fmi.mjt.carstore.enums;

import java.util.Random;

public enum Region {
	SOFIA("CB"), 
	BURGAS("A"), 
	VARNA("B"),
	PLOVDIV("PB"), 
	RUSE("P"), 
	GABROVO("EB"),
	VIDIN("BH"),
	VRATSA("BP");
	
	private String prefix;
	private int number = 1000;
	
	private Region(String prefix) {
		this.prefix = prefix;
	}
	
	public String getPrefix() {
		return this.prefix;
	}
	
	public void increaseNumber() {
		number++;
	}
	
	public int getNumber() {
		return this.number;
	}
	
	/*public String getRegistrationNumber() {
		String registrationNumber = prefix + number + getRandomChar() + getRandomChar();
		number++; 
		return registrationNumber;
	}*/
}
