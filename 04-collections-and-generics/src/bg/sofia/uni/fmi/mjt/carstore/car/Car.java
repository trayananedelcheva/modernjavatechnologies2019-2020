package bg.sofia.uni.fmi.mjt.carstore.car;

import bg.sofia.uni.fmi.mjt.carstore.enums.EngineType;
import bg.sofia.uni.fmi.mjt.carstore.enums.Model;
import bg.sofia.uni.fmi.mjt.carstore.enums.Region;

public abstract class Car implements Comparable<Car> {
	private Model model; 
	private int year; 
	private int price; 
	private EngineType engineType;
	private String registrationNumber;
	private Region region;
	RegistrationNumberGenerator generateRegistrationNumber = new RegistrationNumberGenerator();
	
	protected Car(Model model, int year, int price, EngineType engineType, Region region) {
		this.model = model; 
		this.year = year; 
		this.price = price;
		this.engineType = engineType;
		this.region = region;
		this.registrationNumber = generateRegistrationNumber.generateRegistrationNumber(region);
	}
	
	public void setModel(Model model) {
		this.model = model;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public void setEngineType(EngineType engineType) {
		this.engineType = engineType;
	}
	
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	
	public Model getModel() {
		return this.model;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public EngineType getEngineType() {
		return this.engineType;
	}
	
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	
	public Region getRegion() {
		return this.region;
	}
	
	public void setRegion(Region region) {
		this.region = region;
	}
	
	@Override
	public final boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		if(!(o instanceof Car)) {
			return false; 
		}
		Car other = (Car)o;
		return this.registrationNumber.equals(other.registrationNumber);
	}
	
	@Override
	public String toString() {
		return model + " " + year + " " + price +  " " + engineType + " " + region; 
	}
	
	@Override
	public final int hashCode() {
		int result = 1; 
		final int PRIME = 31; 
		if(this.registrationNumber != null) {
			result = PRIME * result + this.registrationNumber.hashCode();
		}
		return result;
	}
	
	@Override
	public int compareTo(Car otherCar) {
		return (this.getRegistrationNumber().hashCode() - otherCar.getRegistrationNumber().hashCode());
	}
}
