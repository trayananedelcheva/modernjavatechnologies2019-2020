package ex;

public class SortingAndSearchingAlgorithms{
	
	public static void bubbleSort(int[] arr) {
		for(int i = 0; i < arr.length - 1; i++) {
			boolean flag = false;
			for(int j = 0; j < arr.length - i - 1; j++) {
				if(arr[j] > arr[j + 1]) {
					int temp = arr[j+1]; 
					arr[j+1] = arr[j]; 
					arr[j] = temp;
					flag = true;
				}
			}
			if(flag == false) {
				break;
			}
		}
	}
	
	public static void insertionSort(int[] arr) {
		for(int i = 1; i < arr.length; i++) {
			int key = arr[i]; 
			int j = i - 1; 
			while(j >= 0 && arr[j] > key) {
				arr[j+1] = arr[j]; 
				j = j - 1;
			}
			arr[j+1] = key;
		}
	}
	
	public static void selectionSort(int[] arr) {
		for(int i = 0; i < arr.length - 1; i++) {
			int min = i; 
			for(int j = i + 1; j < arr.length; j++) {
				if(arr[min] > arr[j]) {
					min = j;
				}
			}
			int temp = arr[i]; 
			arr[i] = arr[min]; 
			arr[min] = temp;
		}
	}
	
	public static boolean binarySearch(int[] arr, int l, int r, int x) {
		if(l <= r) {
			int m = l + (r - l) / 2; 
			if(x == arr[m]) {
				return true;
			}
			else if (arr[m] > x) {
				return binarySearch(arr, l, m - 1, x);
			}
			else if(arr[m] < x) {
				return binarySearch(arr, m + 1, r, x);
			}
			else {
				return false;
			}
		}
		return false;
	}
	
	public static boolean ternarySearch(int[] arr, int l, int r, int x) {
		if(l <= r) {
			int m1 = (l + (r - l)) / 3; 
			int m2 = (r - (r - l)) / 3;
			if(x == arr[m1]) {
				return true;
			}
			if(x == arr[m2]) {
				return true;
			}
			if(x < arr[m1]) {
				return ternarySearch(arr, l, m1 - 1, x);
			}
			if(x > arr[m2]) {
				return ternarySearch(arr, m2 + 1, r, x);
			}
			else {
				return ternarySearch(arr, m1 + 2, m2 - 1, x);
			}
		}
		return false;
	}
	
	public static boolean linearSearch(int[] arr, int x) {
		for(int i = 0; i < arr.length; i++) {
			if(arr[i] == x) {
				return true;
			}
		}
		return false;
	}
	
	private static void merge(int[] arr, int[] aux, int l, int r, int m) {
		int i = l;
		int j = m + 1; 
		int k = l; 
		while(i <= m && j <= r) {
			if(arr[i] < arr[j]) {
				aux[k] = arr[i];
				i++;
				k++;
			}
			else {
				aux[k] = arr[j]; 
				j++;
				k++;
			}
		}
		while(i <= m) {
			aux[k] = arr[i];
			i++;
			k++;
		}
		while(j <= r) {
			aux[k] = arr[j]; 
			j++;
			k++;
		}
		for(int p = l; p <= r; p++) {
			arr[p] = aux[p];
		}
	}
	
	public static void mergeSort(int[] arr, int l, int r) {
		if(l >= r) {
			return;
		}
		int m = l + (r - l) / 2;
		int[] aux = new int[arr.length];
		mergeSort(arr, l, m - 1); 
		mergeSort(arr, m + 1, r);
		merge(arr, aux, l, r, m);
	}
	
	private static int partition(int[] arr, int l, int r) {
		int pivot = l; 
		int storeIndex = pivot + 1; 
		for(int j = pivot + 1; j < arr.length; j++) {
			if(arr[pivot] > arr[j]) {
				int temp = arr[storeIndex]; 
				arr[storeIndex] = arr[j];
				arr[j] = temp;
				storeIndex++;
			}
		}
		int temp = arr[storeIndex - 1];
		arr[storeIndex - 1] = arr[pivot]; 
		arr[pivot] = temp; 
		pivot = storeIndex - 1; 
		return pivot;
	}
	
	public static void quickSort(int[] arr, int l, int r) {
		if(l >= r) {
			return;
		}
		int pivot = partition(arr, l, r); 
		quickSort(arr, l, pivot - 1);
		quickSort(arr, pivot + 1, r);
	}
	
	public static void main(String[] args) {
		int[] arr = new int[] {5, 7, 9, 1, 3, 11};
		//bubbleSort(arr);
		//insertionSort(arr);
		//selectionSort(arr);
		//System.out.println(linearSearch(arr, 3));
		//System.out.println(binarySearch(arr, 0, arr.length - 1, 3));
		//System.out.println(ternarySearch(arr, 0, arr.length - 1, 3));
		mergeSort(arr, 0, arr.length - 1);
		for(int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}
}