package ex;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTree {
	public static class Node {
		int value; 
		Node left; 
		Node right; 
		public Node(int value) {
			this.value = value;
			this.left = null; 
			this.right = null;
		}
	}
	
	Node root; 
	
	private Node insert(Node current, int value) {
		if(current == null) {
			return new Node(value);
		}
		else if(value < current.value) { 
			current.left = insert(current.left, value);
		}
		else if(value > current.value) {
			current.right = insert(current.right, value);
		}
		else {
			return current;
		}
		return current;
	}
	
	public void insertNode(int value) {
		this.root = insert(root, value);
	}
	
	private boolean contains(Node current, int value) {
		if(current == null) {
			return false;
		}
		else if(current.value == value) {
			return true;
		}
		else {
			return value > current.value ? contains(current.right, value) : contains(current.left, value);
		}
	}
	
	public boolean containsNode(Node root, int value) {
		return contains(root, value);
	}
	
	private int minValue(Node current) {
		int minValue = current.value;
		while(current != null) {
			minValue = current.left.value;
			current = current.left;
		}
		return minValue;
	}
	
	public Node delete(Node current, int value) {
		if(current == null) {
			return root;
		}
		else if(value < current.value) {
			current.left = delete(current.left, value);
		}
		else if(value > current.value) {
			current.right = delete(current.right, value);
		}
		else if(value == current.value) {
			if(current.left == null) {
				return current.right;
			}
			else if(current.right == null) {
				return current.left;
			}
			else {
				current.value = minValue(current.right);
				current.right = delete(current.right, current.value);
			}
		}
		return current;
	}
	
	public void traverseInOrder(Node root) {
		if(root != null) {
			traverseInOrder(root.left); 
			System.out.print(root.value + " ");
			traverseInOrder(root.right);
		}
	}
	
	public void inOrderTraversal() {
		traverseInOrder(root);
	}
	
	public void traversePreOrder(Node root) {
		if(root != null) {
			System.out.print(root.value + " ");
			traversePreOrder(root.left);
			traversePreOrder(root.right);
		}
	}
	
	public void preOrderTraversal() {
		traversePreOrder(root);
	}
	
	public void traversePostOrder(Node root) {
		if(root != null) {
			traversePreOrder(root.left);
			traversePreOrder(root.right);
			System.out.print(root.value + " ");
		}
	}
	
	public void postOrderTraversal() {
		traversePostOrder(root);
	}
	
	public void deleteNode(Node root, int value) {
		root = delete(root, value);
	}
	
	public void levelOrderTraversal() {
		Queue<Node> queue = new LinkedList<>();
		if(root == null) {
			return;
		}
		queue.add(root);
		while(!queue.isEmpty()) {
			Node n = queue.remove();
			System.out.print(n.value + " ");
			if(n.left != null) {
				queue.add(n.left);
			}
			if(n.right != null) {
				queue.add(n.right);
			}
		}
	}
	
	public static void main(String[] args) {
		BinarySearchTree BST = new BinarySearchTree(); 
		BST.insertNode(6);
		BST.insertNode(4);
		BST.insertNode(8);
		BST.insertNode(3);
		BST.insertNode(5);
		BST.insertNode(7);
		BST.insertNode(9);
		BST.inOrderTraversal();
		System.out.println();
		//BST.delete(5); 
		//BST.inOrderTraversal();
		//System.out.println();
		BST.postOrderTraversal();
		System.out.println();
		BST.preOrderTraversal();
		System.out.println();
		BST.levelOrderTraversal();
	}
}
