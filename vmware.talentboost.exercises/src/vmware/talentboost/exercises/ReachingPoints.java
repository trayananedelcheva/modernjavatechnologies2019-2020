package vmware.talentboost.exercises;

import java.util.Scanner;

public class ReachingPoints {
	
	public static String canReach(int x1, int y1, int x2, int y2) {
		if(x1 == x2 && y1 == y2) {
			return "Yes";
		}
		else if(x2 < x1 || y2 < y1) {
			return "No";
		}
		else {
			String canReach1 = canReach(x1, y1 + x1, x2, y2);
			String canReach2 = canReach(x1 + y1, y1, x2, y2);
			if(canReach1.contains("Yes") || canReach2.contains("Yes")) {
				return "Yes";
			}
		}
		return "No";
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x1 = sc.nextInt(); 
		int y1 = sc.nextInt(); 
		int x2 = sc.nextInt(); 
		int y2 = sc.nextInt();
		System.out.println(canReach(x1, y1, x2, y2));
	}
}
