package vmware.talentboost.exercises;

import java.util.Scanner;

public class CarInheritance {
	abstract static class Car {
		protected boolean isSedan;
		protected String seats;

		public Car(boolean isSedan, String seats) {
			this.isSedan = isSedan;
			this.seats = seats;
		}

		public boolean getIsSedan() {
			return this.isSedan;
		}

		public String getSeats() {
			return this.seats;
		}

		abstract public String getMileage();

		public void printCar(String name) {
			System.out.println("A " + name + " is " + (this.getIsSedan() ? "" : "not ") + "Sedan, is " + this.getSeats()
					+ "-seater, and has a mileage of around " + this.getMileage() + ".");
		}
	}

	public static class WagonR extends Car {
		Integer mileage;
		
		public WagonR(int mileage) {
			super(false, "4");
			this.mileage = mileage;
		}
		
		public String getMileage() {
			return mileage.toString() + " kmpl";
		}
	}
	
	public static class HondaCity extends Car {
		Integer mileage; 
		
		public HondaCity(Integer mileage) {
			super(true, "4"); 
			this.mileage = mileage;
		}
		
		public String getMileage() {
			return mileage.toString() + " kmpl";
		}
	}
	
	public static class InnovaCrysta extends Car {
		Integer mileage;
		
		public InnovaCrysta(Integer mileage) {
			super(false, "6");
			this.mileage = mileage;
		}
		
		public String getMileage() {
			return mileage.toString() + " kmpl";
		}
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		int carType = sc.nextInt(); 
		int carMileage = sc.nextInt();
		
		if(carType == 0) {
			Car wagonR = new WagonR(carMileage); 
			wagonR.printCar("WagonR");
		}
		
		if(carType == 1) {
			Car hondaCity = new HondaCity(carMileage); 
			hondaCity.printCar("HondaCity");
		}
		
		if(carType == 2) {
			Car innovaCrysta = new InnovaCrysta(carMileage); 
			innovaCrysta.printCar("InnovaCrysta");
		}
	}
}
