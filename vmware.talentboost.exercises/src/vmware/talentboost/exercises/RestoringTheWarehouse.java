package vmware.talentboost.exercises;

import java.io.*;
import java.util.*;

public class RestoringTheWarehouse {
	/*
     * Complete the 'restock' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY itemCount
     *  2. INTEGER target
     */

    public static int restock(List<Integer> itemCount, int target) {
    	int result = 0;
    	for(int i = 0; i < itemCount.size(); i++) {
    			result = result + itemCount.get(i);
    	}
    	if(result >= target) {
			return result - target;
		}
		if(result < target) {
			return target - result;
		}
		return result;
    }
    
    public static void main(String[] args) throws IOException {
            Scanner sc = new Scanner(System.in);
            List<Integer> itemCount = new ArrayList<>();
            int n = sc.nextInt();
            for(int i = 0; i < n; i++) { 
            	itemCount.add(sc.nextInt());
            }
            int target = sc.nextInt(); 
            System.out.println(restock(itemCount, target));
     }
}
