package vmware.talentboost.exercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CountingPairs {
	
	public static int countPairs(List<Integer> numbers, int k) {
		List<Integer> numberWithoutDuplicates = numbers.stream().distinct().sorted().collect(Collectors.toList());
		int numOfPairs = 0;
		for(int i = 0; i < numberWithoutDuplicates.size()-1; i++) {
			//System.out.println(numberWithoutDuplicates.get(i));
			for(int j = i + 1; j < numberWithoutDuplicates.size(); j++) {
				if(numberWithoutDuplicates.get(i) + k == numberWithoutDuplicates.get(j)) {
					numOfPairs++;
				}
			}
		}
		return numOfPairs;
	}
	
	public static int countPairs1(List<Integer> numbers, int k) {
		Map<Integer, Boolean> map = new TreeMap<Integer, Boolean>();
		int count = 0;
		for(int i = 0; i < numbers.size(); i++) {
			map.put(numbers.get(i), true);
		}
		for(Integer key : map.keySet()) {
			if(map.keySet().contains(key+k)) {
				count++;
			}
		}
		return count;
	}
	
	private static boolean binarySearch(List<Integer> numbers, int l, int n, int x) {
		if(l <= n) {
			int m = (l + (n - l)) / 2; 
			if(x == numbers.get(m)) {
				return true;
			}
			else if(x > numbers.get(m)) {
				return binarySearch(numbers, (m + 1), n, x);
			}
			else if(x < numbers.get(m)) {
				return binarySearch(numbers, l, (m - 1), x);
			}
			else {
				return false;
			}
		}
		return false;
	}
	
	public static int countPairs2(List<Integer> numbers, int k) {
		int count = 0;
		Collections.sort(numbers);
		for(int i = 0; i < numbers.size()-1; i++) {
			if(binarySearch(numbers, i+1, numbers.size() - 1, numbers.get(i) + k) == true) {
				count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		List<Integer> numbers = new ArrayList<Integer>();
		for(int i = 0; i < n; i++) {
			numbers.add(sc.nextInt());
		}
		int k = sc.nextInt();
		//System.out.println(countPairs(numbers, k));
		//System.out.println(countPairs1(numbers, k));
		System.out.println(countPairs2(numbers, k));
		sc.close();
	}
}
