package bg.sofia.uni.fmi.mjt.smartcity;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Comparator;

import bg.sofia.uni.fmi.mjt.smartcity.device.Device;

public class ComparatorOfElementsByPowerConsumption implements Comparator<Device> {
	@Override
	public int compare(Device device1, Device device2) {
		LocalDateTime currentTime = LocalDateTime.now(); 
		double totalPowerConsumptionOfFirstDevice = getTotalPowerConsumption(device1.getInstallationDateTime(), currentTime, device1.getPowerConsumption()); 
		double totalPowerConsumptionOfSecondDevice = getTotalPowerConsumption(device2.getInstallationDateTime(), currentTime, device2.getPowerConsumption());
		int result = Double.compare(totalPowerConsumptionOfSecondDevice, totalPowerConsumptionOfFirstDevice); 
		return result;
	}
	
	public double getTotalPowerConsumption(LocalDateTime installationDateTime, LocalDateTime currentTime, double powerConsumption) {
		double lifeExpectancy = Duration.between(installationDateTime, currentTime).toMinutes
				();
		powerConsumption = powerConsumption * 0.0166666667;
		double totalPowerConsumption = lifeExpectancy * powerConsumption;
		return totalPowerConsumption;
	}
}
