package bg.sofia.uni.fmi.mjt.smartcity.device;

import java.time.LocalDateTime;

public class SmartTrafficLight extends Device implements SmartDevice {
	public SmartTrafficLight(String name, double powerConsumption, LocalDateTime installationDateTime) {
		super(name, powerConsumption, installationDateTime);
	}
}
