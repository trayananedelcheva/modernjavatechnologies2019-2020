package bg.sofia.uni.fmi.mjt.smartcity.hub;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import bg.sofia.uni.fmi.mjt.smartcity.ComparatorOfElementsByPowerConsumption;
import bg.sofia.uni.fmi.mjt.smartcity.device.Device;
import bg.sofia.uni.fmi.mjt.smartcity.device.SmartCamera;
import bg.sofia.uni.fmi.mjt.smartcity.device.SmartDevice;
import bg.sofia.uni.fmi.mjt.smartcity.device.SmartLamp;
import bg.sofia.uni.fmi.mjt.smartcity.device.SmartTrafficLight;
import bg.sofia.uni.fmi.mjt.smartcity.enums.DeviceType;

public class SmartCityHub {
	public static Set<Device> registeredDevices = new LinkedHashSet<>();
	public static Set<Device> registeredDevicesOrderedByPowerConsumption = new TreeSet<>(new ComparatorOfElementsByPowerConsumption());
	public static List<SmartDevice> allDevices = new ArrayList<SmartDevice>();
	int numberOfSmartCameras = 0; 
	int numberOfSmartTrafficLights = 0; 
	int numberOfSmartLamps = 0;
	int totalNumberOfSmartDevices = 0;
	int currentNumber = 0;
	
	public SmartCityHub() {
		registeredDevices = new LinkedHashSet<>();
		registeredDevicesOrderedByPowerConsumption = new TreeSet<>(new ComparatorOfElementsByPowerConsumption());
	}
	
	public int incrementTheNumberOfDevicesOfParticularType(SmartDevice device) {
		if(device instanceof SmartCamera) {
			numberOfSmartCameras++;
			totalNumberOfSmartDevices++;
			return numberOfSmartCameras;
		}
		else if(device instanceof SmartTrafficLight) {
			numberOfSmartTrafficLights++;
			totalNumberOfSmartDevices++;
			return numberOfSmartTrafficLights;
		}
		else if(device instanceof SmartLamp) {
			numberOfSmartLamps++;
			totalNumberOfSmartDevices++;
			return numberOfSmartLamps;
		}
		else {
			return 0;
		}
	}
	
	public void determinateTheType(SmartDevice device) {
		if(device instanceof SmartCamera) {
			SmartCamera smartCamera = (SmartCamera) device; 
			smartCamera.setType(DeviceType.CAMERA);
		}
		if(device instanceof SmartLamp) {
			SmartLamp smartLamp = (SmartLamp) device;
			smartLamp.setType(DeviceType.LAMP);
		}
		if(device instanceof SmartTrafficLight) {
			SmartTrafficLight smartTrafficLight = (SmartTrafficLight) device;
			smartTrafficLight.setType(DeviceType.TRAFFIC_LIGHT);
		}
	}
	
	public String generateId(SmartDevice device) {
		String id = device.getType().getShortName() + "-" + device.getName() + "-" + incrementTheNumberOfDevicesOfParticularType(device);
		return id;
	}
	
	/**
	 * Add a @device to the SmartCityHub.
	 * 
	 * @throws IllegalArgumentException in case @device is null.
	 * @throws DeviceAlreadyRegisteredException in case the @device is already registered.
	 */
	public void register(SmartDevice smartDevice) throws DeviceAlreadyRegisteredException {
		/*throw new UnsupportedOperationException();*/
		if(smartDevice == null) {
			throw new IllegalArgumentException();
		}
		//allDevices.add(smartDevice);
		//System.out.println(currentNumber);
		determinateTheType(smartDevice);
		/*for(int i = 0; i < allDevices.size() - 1; i++) {
			if(allDevices.get(i).getName().equals(allDevices.get(k).getName()) && (allDevices.get(i).getPowerConsumption() == allDevices.get(k).getPowerConsumption())) {
				throw new DeviceAlreadyRegisteredException("Device is already added! ");
			}
			else {
				allDevice
				System.out.println("here");
			 }
		}*/
		if(smartDevice.getType() == DeviceType.CAMERA) {
			SmartCamera smartCamera = (SmartCamera) smartDevice;
			smartCamera.setId(generateId(smartCamera));
			registeredDevices.add(smartCamera);
			registeredDevicesOrderedByPowerConsumption.add(smartCamera);
		}
		if(smartDevice.getType() == DeviceType.LAMP) {
			SmartLamp smartLamp = (SmartLamp) smartDevice;
			smartLamp.setId(generateId(smartLamp));
			registeredDevices.add(smartLamp);
			registeredDevicesOrderedByPowerConsumption.add(smartLamp);
		}
		if(smartDevice.getType() == DeviceType.TRAFFIC_LIGHT) {
			SmartTrafficLight smartTrafficLight = (SmartTrafficLight) smartDevice;
			smartTrafficLight.setId(generateId(smartTrafficLight));
			registeredDevices.add(smartTrafficLight);
			registeredDevicesOrderedByPowerConsumption.add(smartTrafficLight);
		}
	}
	
	public int decrementTheNumberOfDevicesOfParticularType(SmartDevice device) {
		if(device instanceof SmartCamera) {
			numberOfSmartCameras--;
			totalNumberOfSmartDevices--;
			return numberOfSmartCameras;
		}
		else if(device instanceof SmartTrafficLight) {
			numberOfSmartTrafficLights--;
			totalNumberOfSmartDevices--;
			return numberOfSmartTrafficLights;
		}
		else if(device instanceof SmartLamp) {
			numberOfSmartLamps--;
			totalNumberOfSmartDevices--;
			return numberOfSmartLamps;
		}
		else {
			return 0;
		}
	}
	
	/**
	 * Removes the @device from the SmartCityHub.
	 * 
	 * @throws IllegalArgumentException in case null is passed. 
	 * @throws DeviceNotFoundException in case the @device is not found.
	 */
	public void unregister(SmartDevice device) throws DeviceNotFoundException {
		if(device == null) {
			throw new IllegalArgumentException();
		}
		 
		if(registeredDevices.contains(device) == false) {
			throw new DeviceNotFoundException("This device cannot be found in registered devices! ");
		}
		else {
			registeredDevices.remove(device);
			decrementTheNumberOfDevicesOfParticularType(device);
		}
	}
	
	/**
	 * Returns SmartDevice with an ID @id.
	 * 
	 * @throws IllegalArgumentException in case @id is null.
	 * @throws DeviceNotFoundException in case device with ID @id is not found.
	 */
	public SmartDevice getDeviceById(String id) throws DeviceNotFoundException {
		if(id == null) {
			throw new IllegalArgumentException();
		}
		for(SmartDevice device: registeredDevices) {
			if(id.equals(device.getId())) {
				return device;
			}
			else {
				throw new DeviceNotFoundException("This divece doent't exist in registered devices! ");
			}
		}
		return null;
	}
	
	/**
	 * Returns the total number of devices with type @type registered in SmartCityHub.
	 * 
	 * @throws IllegalArgumentException in case @type is null.
	 */
	public int getDeviceQuantityPerType(DeviceType type) {
		if(type == null) {
			throw new IllegalArgumentException();
		}
		if(type == DeviceType.CAMERA) {
			return numberOfSmartCameras;
		}
		if(type == DeviceType.LAMP) {
			return numberOfSmartLamps;
		}
		if(type == DeviceType.TRAFFIC_LIGHT) {
			return numberOfSmartTrafficLights;
		}
		return 0;
	}
	
	/**
	 * Returns a collection of Ids of top @n devices which consumed
	 * the most power from the time of their installation until now.
	 * 
	 * The total power consumption of a device is calculated by the hours elapsed
	 * between the two LocalDateTime-s: the installation time and the current time (now)
	 * multiplied by the Stated nominal hourly power consumption of the device.
	 * 
	 * If @n exceeds the total number of devices, return all devices available sorted by the given criterion.
	 * @throws IllegalArgumentException in case @n is a negative number.
	 */
	public Collection<String> getTopDevicesByPowerConsumption(int n) {
		List<String> allDevicesIds = new ArrayList<>();
		//List<SmartDevice> allDevices = new ArrayList<>(); 
		if(n < 0) {
			throw new IllegalArgumentException();
		}
		else if(n > registeredDevicesOrderedByPowerConsumption.size()) {
			for(SmartDevice device: registeredDevicesOrderedByPowerConsumption) {
				allDevicesIds.add(device.getId());
			}
		}
		else {
			allDevicesIds = registeredDevicesOrderedByPowerConsumption.stream().map(Device::getId).limit(n).collect(Collectors.toList());
		}
		return allDevicesIds;
	}
	
	/**
	 * Returns a collection of the first @n registered devices, i.e. the first @n that were added
	 * in the SmartCityHub (registration != installation).
	 * 
	 * If @n exceeds the total number of devices, return all devices available sorted by the given criterion.
	 * 
	 * @throws IllegalArgumentException in case @n is a negative number.
	 */
	public Collection<SmartDevice> getFirstDevicesByRegistration(int n) {
		List<SmartDevice> registeredNDevices = new ArrayList<>();
		if(n < 0) {
			throw new IllegalArgumentException();
		}
		else if(n > registeredDevices.size()) {
			for(SmartDevice device: registeredDevices) {
				registeredNDevices.add(device);
			}
		}
		else {
			registeredNDevices = registeredDevices.stream().limit(n).collect(Collectors.toList());
		}
		return registeredNDevices;
	}
	
	public static void main(String[] args) {
		SmartCityHub smartCityHub = new SmartCityHub(); 
		SmartCamera device1 = new SmartCamera("SofiaUniversitySmartCamera", 65, LocalDateTime.of(2015, Month.JULY, 29, 19, 30, 40));
		SmartCamera device2 = new SmartCamera("FacultyOfMathematicsAndInformaticsSmartCameraEntrance", 65, LocalDateTime.of(2009, Month.JUNE, 15, 10, 26, 40));
		SmartCamera device3 = new SmartCamera("LionsBridgeSmartCamera1", 65, LocalDateTime.of(2018, Month.JANUARY, 2, 10, 15, 36));
		SmartCamera device4 = new SmartCamera("LionsBridgeSmartCamera2", 65, LocalDateTime.of(2018, Month.JANUARY, 2, 10, 35, 50));
		SmartCamera device5 = new SmartCamera("LionsBridgeSmartCamera3", 65, LocalDateTime.of(2018, Month.JANUARY, 2, 10, 50, 54));
		//SmartCamera device6 = new SmartCamera("LionsBridgeSmartCamera4", 65, LocalDateTime.of(2018, Month.JANUARY, 2, 11, 11, 41));
		//SmartCamera device7 = new SmartCamera("TokudaSmartCamera", 65, LocalDateTime.of(2017, Month.MARCH, 5, 9, 54, 18));
		smartCityHub.register(device1); 
		//System.out.println(device1.getType());
		//
		//smartCityHub.register(device1);
		smartCityHub.register(device2);
		smartCityHub.register(device3);
		smartCityHub.register(device4); 
		smartCityHub.register(device5);
		for(SmartDevice device: registeredDevices) {
			System.out.println(device.toString());
		}
		System.out.println("-----------------------------------------------------");
		List<String> allDeviesIds = registeredDevicesOrderedByPowerConsumption.stream().map(Device::getId).limit(10).collect(Collectors.toList());
		for(String id: allDeviesIds) {
			System.out.println(id);
		}
		for(SmartDevice device: registeredDevicesOrderedByPowerConsumption) {
			System.out.println(device.toString());
		}
	}
}
