package bg.sofia.uni.fmi.mjt.smartcity.hub;

public class DeviceAlreadyRegisteredException extends RuntimeException {
	
	private static final long serialVersionUID = 1460125818886683558L;
	
	public DeviceAlreadyRegisteredException() {
		// TODO Auto-generated constructor stub
	}
	public DeviceAlreadyRegisteredException(String message) {
		super(message);
	}
}
