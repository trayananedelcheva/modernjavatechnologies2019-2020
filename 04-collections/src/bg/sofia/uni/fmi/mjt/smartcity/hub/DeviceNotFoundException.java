package bg.sofia.uni.fmi.mjt.smartcity.hub;

public class DeviceNotFoundException extends Throwable {
	private static final long serialVersionUID = 4125965356358329466L;
	
	public DeviceNotFoundException() {
		// TODO Auto-generated constructor stub
	}
	
	public DeviceNotFoundException(String message) {
		super(message);
	}
}
