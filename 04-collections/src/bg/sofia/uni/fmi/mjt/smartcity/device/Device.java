package bg.sofia.uni.fmi.mjt.smartcity.device;

import java.time.LocalDateTime;

import bg.sofia.uni.fmi.mjt.smartcity.enums.DeviceType;

public class Device implements SmartDevice, Comparable<Object> {
	private String id; 
	private String name; 
	private double powerConsumption;
	private LocalDateTime installationDateTime;
	private DeviceType type;
	
	public Device(String name, double powerConsumption, LocalDateTime installationDateTime) {
		this.name = name; 
		this.powerConsumption = powerConsumption; 
		this.installationDateTime = installationDateTime; 
	}
	
	public String getId() {
		return this.id; 
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getPowerConsumption() {
		return this.powerConsumption;
	}
	
	public LocalDateTime getInstallationDateTime() {
		return this.installationDateTime;
	}
	
	public DeviceType getType() {
		return this.type;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPowerConsumption(double powerConsumption) {
		this.powerConsumption = powerConsumption;
	}
	
	public void setInstallationDateTime(LocalDateTime installationDateTime) {
		this.installationDateTime = installationDateTime;
	}
	
	public void setType(DeviceType type) {
		this.type = type;
	}
	
	/*@Override 
	public boolean equals(Object o) {
		if(o == this) {
			return true;
		}
		if(!(o instanceof Device)) {
			return false;
		}
		Device other = (Device) o;
		return this.id.equals(other.getId());
	}
	
	@Override 
	public final int hashCode() {
		int result = 1;
		final int PRIME = 31;
		if(id != null) {
			result = PRIME * result + this.id.hashCode();
		}
		return result;
	}*/
	
	@Override 
	public String toString() {
		return id + " " + name + " " + powerConsumption + " " + installationDateTime + " " + type;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(Object o) {
		Device device = (Device) o; 
		return (this.id.hashCode() - device.getId().hashCode());
	}
}
